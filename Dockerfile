FROM rust:1.72.0 AS builder
COPY Cargo.lock /app/Cargo.lock
COPY Cargo.toml /app/Cargo.toml
WORKDIR /app
RUN mkdir src && echo "fn main() {}" > src/main.rs
RUN cargo fetch --locked
RUN rm src/main.rs
COPY src /app/src
RUN cargo build -r --locked

FROM fedora:38
RUN dnf in -y openssl1.1 && dnf clean all
COPY --from=builder /app/target/release/semtag /usr/local/bin/semtag
ENTRYPOINT ["/usr/local/bin/semtag"]