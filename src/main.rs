use std::process::exit;
use std::str::FromStr;

use clap::Parser;
use oci_distribution::{Client, Reference, RegistryOperation};
use oci_distribution::secrets::RegistryAuth;
use semver::Version;

#[tokio::main]
async fn main() {
    let args = Args::parse();
    let mut client = Client::default();
    let auth = RegistryAuth::Basic(args.username, args.password);
    for image in args.images.iter() {
        let image = Reference::from_str(image).expect("Failed to Parse Image Tag");
        let result = client
            .list_tags(&image, &auth, None, None)
            .await
            .expect("failed to fetch repo content");
        let version = image.tag().unwrap();
        let version = match Version::parse(version) {
            Ok(version) => version,
            Err(_) => {
                eprintln!("Parsing of Tag '{version}' failed. Only Semver compliant Tags are supported.");
                exit(1);
            }
        };
        if !version.pre.eq_ignore_ascii_case("") && !args.allow_pre_release {
            eprintln!("Tag '{image} is a Pre-Release Version. For Security reasons those are not handled by Default.");
            eprintln!("If you are sure you want to use them anyway you can use the '--allow-pre-release' Flag.");
            eprintln!("Skipping Tag '{image}'.");
            continue;
        }
        let repo_content: Vec<Version> = result
            .tags
            .iter()
            .map(|tag| Version::parse(tag))
            .filter(|version| version.is_ok())
            .map(|version| version.unwrap())
            .collect();
        let manifest = client
            .pull_manifest(&image, &auth)
            .await
            .expect(&*format!("could not pull image {}", image.to_string()));
        client
            .auth(&image, &auth, RegistryOperation::Push)
            .await
            .expect("couldn't authenticate");
        if is_latest(&version, &repo_content) {
            let reference = reference_with_tag(&image, "latest");
            println!("pushing {reference}");
            client
                .push_manifest(&reference, &manifest.0.clone().into())
                .await
                .unwrap();
        }
        if is_latest_major(&version, &repo_content) {
            let reference = reference_with_tag(&image, &*format!("{}", version.major));
            println!("pushing {reference}");
            client
                .push_manifest(&reference, &manifest.0.clone().into())
                .await
                .unwrap();
        }
        if is_latest_minor(&version, &repo_content) {
            let reference =
                reference_with_tag(&image, &*format!("{}.{}", version.major, version.minor));
            println!("pushing {reference}");
            client
                .push_manifest(&reference, &manifest.0.into())
                .await
                .unwrap();
        }
        println!("Finished processing '{image}'")
    }
}

/// Simple Tool to create shorthand Tags from full Semver Image Tags.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// username
    #[arg(short, long)]
    username: String,

    /// password
    #[arg(short, long)]
    password: String,

    /// allow Pre-Release Versions to be processed.
    #[arg(long, default_value_t = false)]
    allow_pre_release: bool,

    #[arg()]
    images: Vec<String>,
}

fn reference_with_tag(reference: &Reference, tag: &str) -> Reference {
    return Reference::with_tag(
        String::from(reference.registry()),
        String::from(reference.repository()),
        String::from(tag),
    );
}

fn is_latest(version: &Version, repo_content: &Vec<Version>) -> bool {
    let mut repo_content = repo_content.iter().collect::<Vec<&Version>>();
    repo_content.sort();
    return repo_content
        .last()
        .expect("Repo does not contain given version")
        .eq(&version);
}

fn is_latest_major(version: &Version, repo_content: &Vec<Version>) -> bool {
    let mut majors = repo_content
        .iter()
        .filter(|repo_ver| repo_ver.major == version.major)
        .collect::<Vec<&Version>>();
    majors.sort();
    return majors
        .last()
        .expect("Repo does not contain given version")
        .eq(&version);
}

fn is_latest_minor(version: &Version, repo_content: &Vec<Version>) -> bool {
    let mut minors = repo_content
        .iter()
        .filter(|repo_ver| repo_ver.major == version.major && repo_ver.minor == version.minor)
        .collect::<Vec<&Version>>();
    minors.sort();
    return minors
        .last()
        .expect("Repo does not contain given version")
        .eq(&version);
}

#[cfg(test)]
mod tests {
    use semver::{BuildMetadata, Prerelease, Version};

    use crate::{is_latest, is_latest_major, is_latest_minor};

    fn repo_content() -> Vec<Version> {
        let mut content: Vec<Version> = Vec::new();
        for major in 1..=3 {
            for minor in 1..=3 {
                for patch in 1..=3 {
                    content.push(Version {
                        major,
                        minor,
                        patch,
                        pre: Prerelease::EMPTY,
                        build: BuildMetadata::EMPTY,
                    })
                }
            }
        }
        content
    }

    #[test]
    fn is_latest_positive() {
        assert!(is_latest(
            &Version::parse("3.3.3").unwrap(),
            &repo_content(),
        ));
    }

    #[test]
    fn is_latest_negative() {
        assert!(!is_latest(
            &Version::parse("2.3.3").unwrap(),
            &repo_content(),
        ));
    }

    #[test]
    fn is_latest_major_positive() {
        assert!(is_latest_major(
            &Version::parse("2.3.3").unwrap(),
            &repo_content(),
        ));
    }

    #[test]
    fn is_latest_major_negative() {
        assert!(!is_latest_major(
            &Version::parse("2.3.2").unwrap(),
            &repo_content(),
        ));
    }

    #[test]
    fn is_latest_minor_positive() {
        assert!(is_latest_minor(
            &Version::parse("2.2.3").unwrap(),
            &repo_content(),
        ));
    }

    #[test]
    fn is_latest_minor_negative() {
        assert!(!is_latest_minor(
            &Version::parse("2.3.2").unwrap(),
            &repo_content(),
        ));
    }
}
