# Semtag
cli tool to create shorthand tags for given [semver](https://semver.org/) docker/oci image tags, taking repo content into account.

Example:
```shell
docker run --rm rbbl/semtag:1 -u username -p password rbbl/genre-police:2.1.1 rbbl/genre-police:1.8.0
```
Output:
```
pushing docker.io/rbbl/genre-police:latest
pushing docker.io/rbbl/genre-police:2
pushing docker.io/rbbl/genre-police:2.1
Finished processing 'docker.io/rbbl/genre-police:2.1.1'
pushing docker.io/rbbl/genre-police:1
pushing docker.io/rbbl/genre-police:1.8
Finished processing 'docker.io/rbbl/genre-police:1.8.0'
```

Gitlab CI Example:
```yaml
create-shorthand-tags:
  stage: publish
  image:
    name: rbbl/semtag:1
    entrypoint:
      - "/bin/bash"
      - "-cl"
  script:
    - semtag -u $DOCKERHUB_USER -p $DOCKERHUB_ACCESS_TOKEN rbbl/semtag:$CI_COMMIT_TAG
```